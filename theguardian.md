# WARNING, THIS PAGE HAVEN'T BEEN PROOF-READED

In this page I'm going to talk about this [The Guardian article](https://www.theguardian.com/world/2021/mar/12/far-right-open-source-technology-censorship).

I will deconstruct this article paragraph by paragraph. I will tell when I disagree and why and when I agree as well.


> On 8 December last year, a Frenchman called Laurent Bachelier gave away a total of 28.5 bitcoins – worth $556,000 – to 22 people. On the same day, he killed himself.

> In suicide notes written in French and English, he explained that the burden of illness (he suffered from a neurological pain disorder) and his loss of hope for the future had led him to despair. After railing against the decline of western civilization and attacks on free speech, he wrote that he had decided to “leave his modest wealth to certain causes and people”.

> Allusions to the “14 words” slogan used by white supremacists offered a clue as to the causes he favored. The beneficiaries of Bachelier’s largesse were all either prominent far-right agitators, or platforms offering them a home. The donations immediately attracted the attention of cybersecurity researchers, extremism watchers and law enforcement officers.

I never heard of that thing. In deed that is kinda sketchy, but I think this should be backed up by a source of some kind. As you will see later on, I would like to know the author's critters for "far right".

> Bachelier gave the video platform BitChute two bitcoins (in January, the price of a single bitcoin ranged between $30,000 and $40,000). The neo-Nazi website the Daily Stormer got one, the French Holocaust denier Vincent Reynouard got 1.5, and the US white nationalist celebrity Nick Fuentes, an attendee of the riots in Charlottesville and the rally that preceded the storming of the Capitol in Washington, received 13.5 – worth over $450,000.

In deed that sucks. I never heard of "BitChute" so I looked it up, and this platform is fucking hillarious by its stupidity. 

The first video's title on the homepage is called "The New World Order Movement is the REAL Virus". That pretty much gives you an idea on the kind of content you could find there.

> A Guardian investigation can now reveal that one of the lesser-known beneficiaries is a YouTube influencer of sorts – one with a history of promoting far-right political ideology. Luke Smith, now a Florida resident, maintains a monetized YouTube channel with 109,000 subscribers. He received at least one bitcoin from Bachelier, valued at the time of writing at just over $30,000.

For more information, Luke Smith is a video creator that mostly creates tutorial about Linux and minimalist software. 

As a personal opinion, his videos are pretty instructives, but he uses some sketchy memes like the "bugman" thing which looks like a super-traditionalist meme. 

He seem to also attract a far-right community on his channel.

> It’s possible that Bachelier saw in Luke Smith a like mind and a shared purpose. Beyond their common ground in far-right politics, each saw technology as a weapon in their war against liberal, tolerant societies.

Luke Smith rarely talk about explicit politics like "right" and "left" or some president. So guessing his political opinion is kinda tricky. 

Also Luke Smith isn't that much against tolerance as far as I know. He is against dependence on high-technologies (that's what he's mostly talking in his videos). 

And on that point I agree with him, we're more and more dependent on technologies that we can't produce ourselves, and consumes a lot of power. 

I don't say high-techs are bad, I'm saying dependence on them is bad.

> Like Bachelier, Smith eschews so-called proprietary software – like MacOS or Microsoft Word – and communications tools like Facebook or Twitter, built and controlled by Silicon Valley firms. Instead, Smith is an advocate for so-called “open source software” – the kind that makes it possible to use, copy, redistribute and modify software legally. And recently, he has been promoting communications platforms that might help extremists to operate beyond the reach of censorship – and even the law.
What Smith preaches: a war against the modern world

I truly hate this argument. Let me explain why: 

First, promoting technologies that **might** be used by far-right, isn't far right. It's as stupid as saying that knives should be banned because they could be used to harm others. 

On that side, that would mean, any privacy advocate is a fascist according to that argument. While, most of the time, those kind of technologies are used by **anti**-fascist communities. 

Second, I never heard Smith wanting a *war against the modern world*. He sure don't want to live in that modern world and be self-sufficient. He also sometimes uses the terms *war against bloat* for reffering to softwares that are are slow and unefficient. And still, this is not something you should take litterally. 

> The man being funded by Bachelier’s donation likes to present himself as a latter-day Ted Kaczynski – the so-called Unabomber, whose infamous manifesto Smith has at times earnestly recommended to his followers.

That is true. But to be clear, the ideas aren't the same as the actions. Ted Kaczynski has a lot of very accurate critiques of our modern society. I highly recommend reading [this article](https://web.archive.org/web/20210308015844/https://www.wired.com/2000/04/joy-2/) and [this other article](https://www.wired.co.uk/article/unabomber-netflix-tv-series-ted-kaczynski).

The first one is very interesting, especially because the best friend of the author was one of Ted Kaczynski's victims.  He hates Ted because Ted hurted his best friend, but still agrees with the ideas Ted describes in his manifesto. 

Also, Ted Kaczynski is not a fascist but an anarcho-primitivist, he critiques the left, but also the right. He actually describes fascism as "kook ideology" and says Nazi ideology is "evil" in one of his letters. 

On a personal note, I agree with Ted on things about the dependence on modern high-tech, non-sustainable technologies. 

But I won't explain more about him, go read those two articles I linked above (if you don't have that much time, just read the second one because the first one is very long). 

> Kaczynski, a terrorist still imprisoned for a 17-year bombing campaign that killed three and injured 23, was motivated by a hatred of the modern technological world. In recent years, his apocalyptic account of an industrial civilization on the brink of collapse has resonated with rightwing extremists – including the Christchurch mosque murderer, Brenton Tarrant – who describe themselves as “eco-fascists”.

Again go read [this article](https://www.wired.co.uk/article/unabomber-netflix-tv-series-ted-kaczynski) for more info about this guy, and for informations on why he attracted "eco-fascists". 

> In 2019, Smith said in a video he wanted to live in a “Unabomber cabin” to escape the surveillance and censorship which he believes is especially aimed at the far right. In a post on his blog in the same year – since deleted – he described the modern world as one “where your every action is watched, if you use proprietary software and communicate only via social media services”.

I watched can't find the video talking about that. He done two videos about the "Unabomber cabin", but those videos are just a description of his dream lifestyle and land. 

He's talking about what kind of house he wants, the size of the land, etc. He didn't talk about the right wing (or the left wing), nor censorship nor surveillance in that video. 

He actually rarely talk about "right" and "left". 

Also, his claim about the modern world has nothing fascist, this is called ads, and that claim have been prooved by Edward Snowden.

> Public records show that Smith moved to a rural property that year near Mayo, in northern Florida, whose title is held by a family member. Since then, most of his videos have been recorded in and around the property.

What's the problem with that, and why is it relevant in this article?

> In various videos and podcasts, Smith rehearses other ideas associated with the far right. He advocates breaking the US up – potentially into racial enclaves “maybe [by] dividing by states, maybe [by] dividing by ethnic groups”. The fantasy of the US splintering along ethnic lines has long been entertained by white nationalists, who have taken to calling themselves the “Balk Right”.

> This is not the only place where Smith touches on ideas associated with white nationalism. In a 2018 podcast, he offers an account of human history that relies on arguments made in The 10,000 Year Explosion, described by the Southern Poverty Law Center as a white nationalist book. Smith also directed readers to websites like radishmag, where readers are asked to “reconsider” slavery and lynching is painted in a positive light.

> Luke Smith did not respond to repeated requests for comment.

In deed, that's lame. I won't check if he really said that, because I have more interesting things to do than listening to a 3 hours podcast. 

> Taken together, these beliefs come back to another far-right splinter ideology: the neoreactionary movement, which in the last decade has been enjoying an online renaissance of sorts, especially among some of Silicon Valley’s tech elite.
The birth of the neoreactionary movement

> The neoreactionary movement traces its history to 2007, when the Silicon Valley entrepreneur Curtis Yarvin started a popular blog under the pseudonym Mencius Moldbug. He used it to attack liberalism, democracy and equality, discussed racial hierarchy in the euphemistic terms of “human biodiversity”, and counseled followers to simply detach themselves from the society ruled by the institutions of liberalism.

> Journalist Corey Pein wrote an account of the culture of Silicon Valley which, in part, examines the influence that Yarvin’s ideas had in the tech world. Pein says that while neoreactionary ideology is somewhat incoherent, what is consistent is the members’ commitment to extricate themselves from liberal democracy. This “exit” doctrine was influential among some Silicon Valley leaders, including the tech billionaire Peter Thiel, who once memorably said: “I no longer believe that freedom and democracy are compatible.”

> Smith follows the same ideological path. His principal outlet for these ideas is his YouTube channel, where he offers tutorials on how to use austere open source software applications, encouraging viewers to detach themselves from Silicon Valley’s products. The channel is both relatively successful and lucrative, and followers rate him highly. His videos have had more than 18.7m views, meaning he could earn anywhere up to $31,100 a year from his channel on current numbers.

HOW IS THIS RELATED TO THE THREE PRECEDENT PARAGRAPHS. I also don't wantot use Silicon Valley's application, because of ads, because of non-ethical policies, because of lack of respect for user's privacy and ton of other reasons.

For the money stuff, I've been chocked. I always go on internet with an ad-blocker. And I just disabled it to check if his channel was monetized, and... It is. I find pretty hypocrite to advocate for privacy and enable ads on its YouTube channel.

>    Smith has been pushing users in the direction of decentralized social media platforms in the so-called 'fediverse'

Well I do as well, and I'm not far-right, not even right, not alt-right, not any of that bullshit.

> YouTube confirmed that Smith’s channel remained in their partner program, meaning that he continues to earn money from the channel, but that they had removed one video, featuring racial slurs, which the Guardian had asked about.

> Smith has lately been pushing users in the direction of decentralized, resilient social media platforms in the so-called “fediverse”, a network of independent social media sites that communicate with one another, and allow people to interact across different sites. This could allow far-right activists to operate in ways that make them very difficult to shut down.

Again, this article is reapeating itself, those platforms are amazing, they work on the same was as emails (emails are decentralized, there is not only one email server, even if most people use Gmail). And I already discussed why this argument is meaningless. 

> Though many prominent programmers and advocates in both the wider open source software movement and the fediverse are motivated by progressive, anti-corporate or anti-authoritarian political ideals, now the tools they have created might be used to shelter far-right extremists from the consequences of their hate speech and organizing.
Manipulating the open source movement for nefarious ends

That's true, and I appreciate the fact they precise this, otherwise a non-tech reader would probably believe fediverse is full of Nazi everywhere. 

While in reality, when you fall on that kind of crap, you usually looked for it.

> The free and open source software movement has attracted many people with progressive politics, who have used it to help provide digital tools to those with few resources, to breathe new life into hardware that might otherwise have been added to a growing mountain of e-waste, or to move public institutions from Barcelona to Brasília away from dependence on expensive software.

> However, experts say that it is not surprising that someone like Smith would be tolerated or even welcomed by some elements of open source culture.

Again, Luke Smith is not the pest, I really don't understand why this article is making a big focus on him. His channel is mainly about his lifestyle and his interests for Linux. 

There are other channels on the fediverse and on YouTube that have for main topic their racist political ideology or conspiracy theory. Why isn't this article talking about those instead?

> Megan Squire is a professor of computer science at Elon University who has published research on both the far right and open source software communities. She says that “the dominant open source culture historically has been one of extreme misogyny, unfounded meritocracy, toxicity and abuse of everyone,” and that Smith is one of those resisting efforts to change that culture.

I only felt that with a minority of individuals. Some individuals thinking they have a "higher IQ" because they use Linux for instance. 

But Luke Smith is mostly laughing about it, he doesn't say he is more intelligent than most people because he uses Linux. I guess if it would be the case, he wouldn't have made a YouTube channel to explain Linux tips and tricks to people.

> In recent years, and especially since the Gamergate movement intensified scrutiny on toxicity in tech, some responded to the blatant sexism, antisemitism and racism online with codes of conduct after realizing this behavior was actually starting to hurt them (Squires says they couldn’t recruit and retain developers).

> The provision of safer online spaces for marginalized groups is a large part of the motivation of many of the people who have created the underlying software. On those platforms, tools for moderation and easy ways to flag sensitive content are baked in by design. But Smith is among a small group who repeatedly rail against the introduction of such codes of conduct within open source projects.

I have no opinion on this. At the same time a COC may be useful to avoid a---- but at the same time, people never read that kind of stuff.

>    Some open source communications platforms do away with the need for servers by implementing a 'peer-to-peer' network

> In a video recorded a week after the Capitol riots, when social media bans were removing rightwingers from Donald Trump down to prevent further violence, Smith said that those who wanted to bypass censorship should use the Twitter-like platform, Pleroma.

> Open source software like Pleroma, Mastodon and Matrix reproduce the functions of Twitter, allowing users to send out brief messages to followers. But their implementation and structure are much more decentralized, allowing anyone to set up their own platform on their own server, after which they can join up, or “federate”, with other such communities.

Except the fact Matrix has nothing to do with Twitter, the rest is true.

> Some open source communications platforms go a step beyond this, and do away with the need for servers altogether by implementing a “peer-to-peer” network. PeerTube, for example, allows users to browse and watch videos in a similar way to YouTube, but instead of streaming it to users from a central server, each user watching a video acts as a relay point.

There are still servers on PeerTube. And the peer-to-peer mode is mainly to improve video stream afaik.

> The technical details are perhaps less important than the practical effect: no one has authority over these platforms: no one owns them. While governments and users can place pressure on the big social media companies to ban problematic users or communities, for better or worse, no one can stop anyone creating their own servers or peer-to-peer networks.

If one server becomes too influent it can be stopped by the authorities in which this server is. But in deed it's harder to moderate than on a centralized platform.

> These technologies, then, are effectively uncensorable. According to a report by Emmi Bevensee, the co-founder of research consultancy Rebellious Data and the social media monitoring tool SMAT, extremists have been advocating, and even developing them, for years.

Again, that doesn't mean those technologies are bad because bad people *could* use it. Also, as said earlier they aren't "uncensorable", they are harder to censor, but it's not impossible.

>        The reason I want it as a trans anti-fascist is the same reason that a Nazi wants it; we just have opposite ends

That's a pretty relevant comment!

> “Every marginalized community knows what it’s like to be systematically deplatformed”, says Bevensee, who uses non-binary pronouns, pointing to the way in which groups such as sex workers have adopted platforms like Mastodon after finding themselves unable to advertise their services.

> But as Bevensee’s report shows, peer-to-peer platforms are a double-edged sword. “The reason I want it as a trans anti-fascist is the same reason that a Nazi wants it; we just have opposite ends,” they explain.

> “You know who really doesn’t understand it? The FBI,” Bevensee adds: “we’re talking about a technology that can’t be subpoenaed. It can’t be surveiled” and, in order to carry out remote surveillance of private chats, “you would have to back door every single device in the world”.

> This opens the way for extremists to propagandize and organize on platforms that are beyond the reach of legal authorities and tech giants alike. After the far right-friendly social media site Gab encountered hosting problems and app store bans, it rebuilt itself on Mastodon’s software, despite determined opposition from the platform’s creators and users.

> Beyond Gab’s ambiguous place in the fediverse, the Guardian found dozens of servers using peer-to-peer, open source tools, which were either exclusively or disproportionately devoted either to far-right politics, or to conspiracy theories that mainstream social media services have previously cracked down on, including coronavirus denialism, “incel” culture and neo-Nazism.

As said before, if you search for it, you will find them. But the most popular servers on the fediverse ban those kind of practices (by just, not adding the Nazi servers into their whitelist).  

If I haven't read that article, I would probably never imagined it existed. 

> With the far right under pressure from mainstream social media companies and internet hosts, this may be just the beginning.

>But experts say that despite their recurrent complaints about Silicon Valley’s platforms, extremists will maintain their foothold in the mainstream for as long as they can. As Squire says of Smith’s internet activity: “Why is he still on YouTube? Because that’s where the eyeballs are, that’s where the money is.”

I'm not sure the main goal of Luke is to make money, he never asked people to subscribe, leave comment or like. Also his audience is very short, the market share of Linux users is below 2% and the amount of Linux users that are interested in the kind of things Luke does on his channel is even smaller.

If Luke was only driven by money, he would probably do gaming videos.