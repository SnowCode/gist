echo "If you have already run 'gpg --gen-key', press ENTER"
read $pause
echo "Downloading keys..."
wget https://cdn.discordapp.com/attachments/787257493616328734/787308717825196032/SnowCode.gpg
wget https://cdn.discordapp.com/attachments/787257493616328734/787308235454808124/nex8192.gpg
wget https://cdn.discordapp.com/attachments/787257493616328734/787306792505835540/lissobone.gpg
echo "All keys have been downloaded."

echo "Importing keys..."
gpg --import *.gpg
echo "All keys imported."

echo "Exporting key"
gpg --export --armor --output mykey.gpg
echo "File 'mykey.gpg' have been generated. Please send it on #cryptography"

