# Blueprint for a humane Discord server
## Server pings
Things you should discuss in pings:

* When organizing big events in the guild.
* If you want to do something that affects a lot of people in the guild, and is hard to reverse when people complain. For instance: removing a channel, banning people, changing rules, etc.

Things you can discuss on a ping:

* You want everyone to be on the same page about a topic or issue.
* If you want to make sure people will fully support what you want to do, you can ping to explain what you will do and ask for feedback.
* If you want to do something that requires help from many people, you can use a ping to convince people to help you.
* If you have issues with the actions of a certain member and you failed to solve it with that member personally or you want the group's opinion on it.

## Admins
The admins exist to make sure the guild survives. The admins are not the leaders of the guild, they are the firemen of the guild. They make sure the members keep respecting each other in a welcoming environment. Apart from that, the admins should be indistinguishable from normal members. The admins don't have any more say over the direction of the guild or the projects in the guild than any other member.

Specifically, the admins has two roles, and for everything that doesn't fall into these roles, the admins are regarded as regular members.

* Maintainers of the infrastructure of the guild. Like the bots, channels, roles, etc.
* Moderators for the people in the guild. When conflict happens that can't be resolved in the group, the admins are responsible for resolving the conflict. A great way to do this is the private talk pattern: go talk to the involved parties in private, listen to them and let them know how the group feels.

It's important for the admins to communicate openly about what they do.

The admins can temporarily ban people from the guild if they think it's necessary for resolving conflict and protecting the guild. If they do that, they need to let the members know who is banned and why. The problem has to be discussed so the members can decide on a permanent solution.

## Guidelines
Without the community, the guild would simply not exist, so it is very important that we as a community collaborate and keep it fun for everyone.

We want you to be a part of it, but you need to do three things.

* Use common sense,
* be excellent to others,
* and don't be an asshole.

People have different realities, values and morals, resulting in different ideas for how to do these three things. To get around these differing realities use empathy, not cunning. Continuously convincing others to see things your way will get you what you want in the short run but can breed resentment in the long run. Going out of your way to understand and to accommodate the other person's point of view strengthens the community itself. The guidelines below describe what the guild thinks it means to use common sense, be excellent to others, and not be an asshole.

First and foremost, the golden rule: treat others the way you want to be treated.

Do not read these guidelines like a lawbook, but read it like a cookbook. It doesn't matter if you use a bit more sugar than the recipe says, as long as your goal is to make the cake better.